import React, {Component} from 'react';
import {AsyncStorage, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Provider} from 'react-redux';
import Login from './app/login'
import Signup from './app/Signup'

import {
    createStackNavigator,
    createAppContainer,
} from 'react-navigation';
import HomeScreen from "./app/home";





const AppNavigator = createStackNavigator(
    {


        Login: Login,
        SignUp : Signup,
        Home: HomeScreen,

    },
    {
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
            initialRouteName: "Home",
            headerBackTitleVisible:false

        }
    },

);

const AppContainer = createAppContainer(AppNavigator);



export default class App extends Component{
    render(){
        return(

                <AppContainer />

        )
    }
}

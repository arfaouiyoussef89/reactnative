import React,{Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    Modal,
    AlertStatic as Alert,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import { AsyncStorage } from 'react-native';


class Signup extends Component {
    static navigationOptions = {
        header: null
    };


    constructor(props) {
        super(props);
this._retrieveData();

        this.state = {
            firstName: '',
            firstNameError:'',
            lastName:'',
            lastNameError:'',
                modalVisible: false,



            email: '',
            emailError:'',
            password: '',
            passwordError: '',


        };



    }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // We have data!!
                this.props.navigation.replace('Home');

                console.log(value);
            }
        } catch (error) {
            // Error retrieving data
        }
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    loginNav(){

        this.props.navigation.replace('Login');


    }

    _userSignup() {
    }





    signUp(){
let test= true;
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.email) === false){

this.setState({emailError:'email invalid'});
            test=false;

        }
        else {
            this.setState({emailError:''});

        }
        if(this.state.lastName.length <3 && this.state.lastName!=""){

            this.setState({lastNameError:'LastName invalid '});
            test=false;

        }
        else {
            this.setState({lastNameError:''});


        }

        if(this.state.firstName.length <3 && this.state.firstName!=""){

            this.setState({firstNameError:'LastName invalid '});

            test=false;
        }

        else {
            this.setState({firstNameError:''});


        }


        if(this.state.lastName===""){

            this.setState({lastNameError:'LastName is Empty'});
            test=false;

        }

        if(this.state.firstName===''){
            this.setState({firstNameError:'first name is empty '});
            test=false;

        }
        if(this.state.password==="" ){

            this.setState({passwordError:'password is Empty'});

            test=false;
        }
        if(this.state.password.length<4&&this.state.password!==0){

            this.setState({passwordError:'password is invalid'});

            test=false;
        }
        else {
            this.setState({passwordError:''});


        }




if(test){
    fetch("http://192.168.1.4:3000/auth/register", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "email": this.state.email,
            "password": this.state.password,
            "firstName":this.state.firstName,
            "lastName":this.state.lastName,

        })
    })
        .then((response) => response.json())
        .then((responseData) => {
if(responseData.success!==false)
{
    alert("success");
    this.props.navigation.replace('Home');

}
else {
    alert("echec");
}
        })
        .done();
}



}


    render() {
        return (
                <ScrollView style={{ backgroundColor:"#008080",flex: 1}}>

            <View >




                <TextInput autoCapitalize="none" keyboardType="email-address"
                           style={{marginTop: 100,borderRadius:50,backgroundColor:"white", marginHorizontal: 40, height: 40}} placeholder="   FirstName"
                           value={this.state.firstName} onChangeText={firstName => this.setState({firstName})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.firstNameError}</Text>



                <TextInput autoCapitalize="none" keyboardType="email-address"
                           style={{marginTop: 20,borderRadius:50 ,backgroundColor:"white",marginHorizontal: 40, height: 40}} placeholder="    LastName"
                           value={this.state.lastName} onChangeText={lastName => this.setState({lastName})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.lastNameError}</Text>

                <TextInput autoCapitalize="none" keyboardType="email-address"
                           style={{marginTop: 20,backgroundColor:"white",borderRadius:50 ,marginHorizontal: 40, height: 40}} placeholder=" E-mail"
                           value={this.state.email} onChangeText={email => this.setState({email})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.emailError}</Text>

                <TextInput autoCapitalize="none" secureTextEntry
                           style={{marginTop: 20,borderRadius:50,backgroundColor:"white", marginHorizontal: 40, height: 40}} placeholder="  Password"
                           value={this.state.password} onChangeText={password => this.setState({password})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.passwordError}</Text>


                <TouchableOpacity onPress={() => this.signUp()}>
                    <Text style={{marginTop: 20, color: 'white', textAlign: 'center'}}>
                        SingnUp

                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.loginNav()}>
                    <Text style={{marginTop: 20, color: 'white', textAlign: 'center'}}>
                        SignIn

                    </Text>
                </TouchableOpacity>

            </View>
                </ScrollView>
        )
    }


}

export  default Signup;

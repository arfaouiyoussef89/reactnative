import React,{Component} from 'react';
import {View,TouchableOpacity,Text,TextInput}  from 'react-native';
import { AsyncStorage } from 'react-native';

class Login extends Component {
    static navigationOptions = {
        header: null
    };
    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== null) {
                // We have data!!
                this.props.navigation.replace('Home');

                console.log(value);
            }
        } catch (error) {
            // Error retrieving data
        }
    };


    constructor(props) {
        super(props);
this._retrieveData();
        this.state = {
            email: '',
            emailError:'',
            password: '',
            passwordError:'',
            token:'',
        };



    }


    SignUpnav(){

    this.props.navigation.replace('SignUp');


}

    _storeData = async () => {
        try {
            await AsyncStorage.setItem('token', this.state.token);
        } catch (error) {
            // Error saving data
        }
    };




    login() {

        let test= true;
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.email) === false){

            this.setState({emailError:'email invalid'});
            test=false;

        }
        else {
            this.setState({emailError:''});

        }




        if(this.state.password==="" ){

            this.setState({passwordError:'password is Empty'});

            test=false;
        }
        if(this.state.password.length<4&&this.state.password!==0){

            this.setState({passwordError:'password is invalid'});

            test=false;
        }
        else {
            this.setState({passwordError:''});


        }
        if(test) {

            fetch("http://192.168.1.4:3000/auth/login", {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "email": this.state.email,
                    "password": this.state.password,

                })
            })
                .then((response) => response.json())
                .then(async (responseData) => {
                    if (responseData.success !== false) {
                        this.props.navigation.replace('Home');
                        try {
                            await AsyncStorage.setItem('token', responseData.token);
                            alert(responseData.success);

                        } catch (error) {
                            // Error saving data
                        }
                    } else {
                        try {
                            await AsyncStorage.setItem('token', false);
                            alert(false);

                        } catch (error) {
                            // Error saving data
                        }
                        alert("echec");
                    }
                })
                .done();

        }
    }



    render() {
        return (
            <View style={{flex: 1,backgroundColor:"#008080"}}>

                <TextInput autoCapitalize="none" keyboardType="email-address"
                           style={{marginTop: 200, borderRadius:50,backgroundColor:"white",marginHorizontal: 40, height: 40}} placeholder="Enter email"
                           value={this.state.email} onChangeText={email => this.setState({email})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.emailError}</Text>

                <TextInput autoCapitalize="none" secureTextEntry
                           style={{marginTop: 20,borderRadius:50,backgroundColor:"white", marginHorizontal: 40, height: 40}} placeholder="Enter password"
                           value={this.state.password} onChangeText={password => this.setState({password})}/>
                <Text style={{color:"red",marginBottom:0,marginTop:0, marginHorizontal: 70}}>{this.state.passwordError}</Text>

                <TouchableOpacity onPress={() => this.login()}>
                    <Text style={{marginTop: 20, color: 'white', textAlign: 'center'}}>
                        SignIn

                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.SignUpnav()}>
                    <Text style={{marginTop: 20, color: 'white', textAlign: 'center'}}>
                        SignUp

                    </Text>
                </TouchableOpacity>



            </View>
        )
    }


}
const mapStateToProps = state => ({
    isLoggedIn:state.auth.isLoggedIn,
    isLoading:state.auth.isLoading,
    userData:state.auth.userData,
    token:state.auth.token,
    error:state.auth.error
});

const mapDispatchToProps = dispatch => ({
    login:(email,password) => dispatch(actions.login({email,password}))

});



export default Login;

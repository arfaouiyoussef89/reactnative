import React, { Component } from 'react';
import { Container, Header, Content, DatePicker, Text } from 'native-base';
import {AsyncStorage, TouchableOpacity, View} from "react-native";

class HomeScreen extends React.Component {


    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('token');
            if (value !== false ) {
                // We have data!!
               this.setState({token:value});

                console.log(value);
            }
            else if (value===false) {
                await   this.props.navigation.replace('Home');

            }
        } catch (error) {
            // Error retrieving data
            // Error retrieving data
        }
    };
    logout = async () => {
        try {
            await AsyncStorage.removeItem('token');
            this.props.navigation.replace('Login');

        } catch (error) {
            // Error retrieving data
            // Error retrieving data
        }
    };

    constructor(props) {

        super(props);
        this.state = {
            token:''
        };

        //  this.logout();
        this._retrieveData();
    }




    render() {
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text>Details Screen</Text>
                <TouchableOpacity onPress={() => this.logout()}>
                    <Text style={{marginTop: 20, color: 'blue', textAlign: 'center'}}>
                        Logout

                    </Text>
                </TouchableOpacity>

            </View>
        );
    }
}
export default HomeScreen;

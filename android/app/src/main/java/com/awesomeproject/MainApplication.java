package com.awesomeproject;

import android.app.Application;
import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;
import android.content.Context;
import android.os.Bundle;
import android.content.Intent;
import com.facebook.react.HeadlessJsTaskService;

import com.facebook.react.ReactApplication;
import com.ninty.system.setting.SystemSettingPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import eu.sigrlami.rnsimdata.RNSimDataReactPackage;
import com.centaurwarchief.smslistener.SmsListenerPackage;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import com.devstepbcn.wifi.AndroidWifiPackage;
import com.voximplant.foregroundservice.VIForegroundServicePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.learnium.RNDeviceInfo.RNDeviceInfo;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new SystemSettingPackage(),
            new RNDeviceInfo(),

            new RNSimDataReactPackage(),
            new SmsListenerPackage(),
            new RNAndroidLocationEnablerPackage(),
            new AndroidWifiPackage(),

            new VIForegroundServicePackage(),
            new RNGestureHandlerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }



}
